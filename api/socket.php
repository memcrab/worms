<?php
error_reporting(E_ALL);
date_default_timezone_set('Asia/Singapore');
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
mb_http_input('UTF-8');
mb_regex_encoding('UTF-8');

require_once __DIR__ . "/vendor/autoload.php";
use Libs\Cache;
use Libs\Config;
use Libs\DB;
use Libs\Queue;
use memCrab\File\Yaml;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Sockets\Classrooms;

$port = $argv[1];
$evn = $argv[2];

// Log::setDefaultRotationHandler("errors");
// ErrorHandler::register(Log::stream("errors"));

require_once __DIR__ . "/Autoloader.php";
$Autoloader = new Autoloader();
$Autoloader->autoloadCore();

$Yaml = new Yaml();
$configuration = $Yaml->load(getenv("CONFIG_PATH"), null)->getContent();
Config::loadConfiguration($configuration);

Cache::obj()->trySetConnect(
    Config::obj()->cache['host'],
    Config::obj()->cache['port'],
    Config::obj()->cache['database'],
    Config::obj()->cache['password']
);

Queue::obj()->setConnect(
    Config::obj()->queue['region'],
    Config::obj()->queue['key'],
    Config::obj()->queue['secret'],
    Config::obj()->queue['version'],
    Config::obj()->queue['endpoint']
);

Queue::obj()->registerQueue("chats-to-db-q", Config::obj()->queue['chats-to-db-q'], [
    'ReceiveMessageWaitTimeSeconds' => 20,
]);

Queue::obj()->registerQueue("whiteboard-to-db-q", Config::obj()->queue['whiteboard-to-db-q'], [
    'ReceiveMessageWaitTimeSeconds' => 20,
]);

DB::obj("tnl")
    ->tryConnect(
        Config::obj()->db['host'],
        Config::obj()->db['user'],
        Config::obj()->db['password'],
        Config::obj()->db['database']
    )
    ->setEncoding(Config::obj()->db['encoding'])
    ->setDebugMode(getenv("DEBUG_MODE"))
    ->setWaitTimeout(Config::obj()->db['wait-timeout']);

$BadWords = new \Factories\BadWords();
$BadWords->fromCache(true)
    ->saveToCache(true)
    ->readFromDatabase();

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new Classrooms($BadWords)
        )
    ),
    $port
);

echo "Listening on {0.0.0.0:" . $port . "}\n";
$server->run();