<?php declare (strict_types = 1);
namespace Sockets;

use Aws\Exception\AwsException;
use Libs\ConnectionsPool;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

class Classrooms implements MessageComponentInterface {
    protected $pool;
    public $BadWords;

    public function __construct(\Factories\BadWords $BadWords) {
        $this->pool = new ConnectionsPool();
        $this->BadWords = $BadWords;
    }

    public function onOpen(ConnectionInterface $conn) {
        try {
            $path = $conn->httpRequest->getUri()->getPath();
            $authString = explode('/', $path);
            $roomId = (isset($authString[1])) ? $authString[1] : null;
            $token = (isset($authString[2])) ? $authString[2] : null;

            if (empty(trim($token))) {
                throw new \Exception("Empty token", 400);
            }

            if (!is_numeric($roomId)) {
                throw new \Exception("Incorrect room", 400);
            } else {
                $roomId = (int) $roomId;
            }

            echo 'TOKEN:' . $token . PHP_EOL;
            echo 'RoomID:' . $roomId . PHP_EOL;
            echo 'Auth process ... ' . PHP_EOL;

            $Auth = \Models\Auth::authByToken($token);

            if ($Auth === false) {
                throw new \Exception("Authentication failed", 401);
            }

            $Events = new \Factories\Events();
            $Events->withTimezone($Auth->getTimezone())
                ->withCountryId($Auth->getCountryId())
                ->withStatuses(['accepted'])
                ->withTrash(0)
                ->withType('classroom')
                ->withId((int) $roomId)
                ->readFromDatabase();

            if (!$Events->total()) {
                throw new \Exception("Classroom not found", 404);
            }

            $Classroom = &$Events->get()[0];

            if ($Auth->getRole() == 'admin') {
                $Users = new \Factories\Users();
                $Users->withId((int) $Auth->getId())
                    ->readFromDatabase();
                if (!($Users->total())) {
                    throw new \Exception("User not found", 404);
                }

                $User = &$Users->get()[0];
                $adminCountryId = $User->getCountryId();

                $Tutor = new \Factories\Users();
                $Tutor->withId((int) $Classroom->getTutorId())
                    ->readFromDatabase();
                if (!($Tutor->total())) {
                    throw new \Exception("User not found", 404);
                }

                $Tutor = &$Tutor->get()[0];
                $tutorCountryId = $Tutor->getCountryId();

                if ($adminCountryId != $tutorCountryId) {
                    throw new \Exception("Admin did not has rights for this classroom", 1);
                }
            } else {
                if (
                    $Classroom->getTutorId() != $Auth->getId() && $Classroom->getStudentId() != $Auth->getId()
                ) {
                    throw new \Exception("User did not relate to this classroom", 1);
                }
            }

            $Chats = new \Factories\Chats();
            $Chats->withRoomId($roomId)
                ->readFromDatabase();

            $conn->send(json_encode([
                "action" => "chat/history",
                "roomId" => $roomId,
                "data" => [
                    'messages' => $Chats->toArray(),
                ],
            ]));

            $Whiteboards = new \Factories\Whiteboards();
            $Whiteboards->withRoomId($roomId)
                ->withLimit(1)
                ->readFromDatabase();

            $whiteboard = [];
            if ($Whiteboards->total() > 0) {
                $whiteboard = $Whiteboards->get()[0]->toArray();
            }

            $conn->send(json_encode([
                "action" => "whiteboard/state",
                "roomId" => $roomId,
                "data" => [
                    'whiteboard' => $whiteboard,
                ],
            ]));

            $this->pool->attach($roomId, $conn);
            $this->pool->setData($roomId, $conn, [
                'auth' => true,
                'token' => $token,
                'userId' => $Auth->getId(),
                'roomId' => $roomId,
            ]);
            echo "New connection! ({$conn->resourceId})\n";
        } catch (\Exception $e) {
            $conn->send(json_encode([
                "action" => "error/message",
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
            ]));

            echo PHP_EOL . $e->getCode() . ':' . $e->getMessage() . PHP_EOL;
            $conn->close();
        }
    }

    public function onMessage(ConnectionInterface $conn, $msg) {
        try {
            echo substr($msg, 0, 50) . PHP_EOL . '------------' . PHP_EOL;
            $data = json_decode($msg, true);

            $roomId = $this->pool->getRoomId($conn);

            if (!isset($data['action'])) {
                throw new \Exception('Bad request', 400);
            }

            switch ($data['action']) {
            case 'chat/post':
                $delivered = 1;
                if (!empty(trim($data['data']['text']))) {
                    $checkMessage = $this->BadWords->checkMessage($data['data']['text']);

                    if (!$checkMessage['success']) {
                        $delivered = 0;
                        $badwords = $checkMessage['message'];
                    }

                    $answerState = [
                        "action" => "chat/delivered",
                        "data" => [
                            'uuid' => $data['data']['uuid'],
                            'senderId' => $data['data']['senderId'],
                            'delivered' => $delivered,
                            'badwords' => isset($badwords) ? $badwords : "",
                        ],
                    ];

                    $conn->send(json_encode($answerState));

                    if (!isset($badwords)) {
                        \Libs\Queue::obj()->sendMessage("chats-to-db-q", [
                            "senderId" => $data['data']['senderId'],
                            "roomId" => $roomId,
                            "timestamp" => time(),
                            "text" => $data['data']['text'],
                        ]);

                        $proxyData = $data;
                        $proxyData['action'] = 'chat/received';
                        $this->pool->sendToRoom($roomId, json_encode($proxyData), $conn);
                    }
                }
                break;
            case 'whiteboard/put':
                $proxyData = $data;
                $proxyData['action'] = 'whiteboard/received';

                $result = \Libs\Queue::obj()->sendMessage("whiteboard-to-db-q", [
                    "roomId" => $roomId,
                    "data" => $data['data'],
                    "action" => 'add/save',
                    "timestamp" => time(),
                ]);

                $answerState = [
                    "action" => "whiteboard/delivered",
                    "data" => [
                        'delivered' => 1,
                        'delivered_patch' => $data['data'],
                        'delivered_canvas' => $data['data']['activeCanvas'],
                    ],
                ];
                $conn->send(json_encode($answerState));
                $this->pool->sendToRoom($roomId, json_encode($proxyData), $conn);
                break;
            case 'whiteboard/cursor/put':
                $answerState = [
                    "action" => "whiteboard/cursor/delivered",
                    "data" => [
                        'delivered' => 1,
                    ],
                ];

                $proxyData = $data;
                $proxyData['action'] = 'whiteboard/cursor/received';

                $conn->send(json_encode($answerState));
                $this->pool->sendToRoom($roomId, json_encode($proxyData), $conn);
                break;
            case 'whiteboard/delete':
                $proxyData = $data;
                $proxyData['action'] = 'whiteboard/delete/received';

                $result = \Libs\Queue::obj()->sendMessage("whiteboard-to-db-q", [
                    "roomId" => $roomId,
                    "data" => $data['data'],
                    "action" => 'delete',
                    "timestamp" => time(),
                ]);

                $answerState = [
                    "action" => "whiteboard/delete/delivered",
                    "data" => [
                        'delivered' => 1,
                    ],
                ];
                $conn->send(json_encode($answerState));
                $this->pool->sendToRoom($roomId, json_encode($proxyData), $conn);
                break;
            case 'whiteboard/state/canvas':
                $Whiteboards = new \Factories\Whiteboards();
                $Whiteboards->withRoomId($roomId)
                    ->withLimit(1)
                    ->readFromDatabase();

                $whiteboard = [];
                if ($Whiteboards->total() > 0) {
                    $canvas = $Whiteboards->get()[0]->toArray();

                    if (isset($canvas['data'][$data['data']['activeCanvas']])) {
                        $whiteboard = $canvas['data'][$data['data']['activeCanvas']];
                    }
                }

                $conn->send(json_encode([
                    "action" => "whiteboard/state/canvas",
                    "activeCanvas" => $data['data']['activeCanvas'],
                    "roomId" => $roomId,
                    "data" => [
                        'whiteboard' => $whiteboard,
                    ],
                ]));
                break;
            default:
                $this->pool->sendToRoom($roomId, json_encode($data), $conn);
                break;
            }
        } catch (AwsException $e) {
            $conn->send(json_encode([
                "action" => "error/message",
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
            ]));

            error_log($e->getMessage());
            echo PHP_EOL . $e->getCode() . ':' . $e->getMessage() . PHP_EOL;
            $conn->close();
        } catch (\Exception $e) {
            $conn->send(json_encode([
                "action" => "error/message",
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
            ]));

            echo PHP_EOL . $e->getCode() . ':' . $e->getMessage() . PHP_EOL;
            $conn->close();
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->pool->detach($conn);
        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}
